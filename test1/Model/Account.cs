﻿using Castle.Components.DictionaryAdapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.Model
{
    public class Account
    {
        public int Id { get; set; }

        public string? Name { get; set; }

        public Client? Client { get; set; }

        public int? ClientsID { get; set; }

        public List<Сard>? Сards { get; set; }
    }
}
