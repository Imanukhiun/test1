﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1.Model
{
    public class Сard
    {
        public int Id { get; set; }

        public string? Name { get; set; }

        public Account? Account { get; set; }

        public int? AccountID { get; set; }
    }
}
