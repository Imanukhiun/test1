﻿using test1.EF;
using test1.Model;

using var db = new EFConnect();

if (db.Clients.FirstOrDefault() == null) {
    Client cl1 = new() { Name = "Иван", Email = "i@yandex.ru" };
    Client cl2 = new() { Name = "Антон", Email = "a@yandex.ru" };
    Client cl3 = new() { Name = "Олег", Email = "o@yandex.ru" };
    Client cl4 = new() { Name = "Сергей", Email = "s@yandex.ru" };
    Client cl5 = new() { Name = "Игорь", Email = "i@yandex.ru" };

    db.Clients.Add(cl1);
    db.Clients.Add(cl2);
    db.Clients.Add(cl3);
    db.Clients.Add(cl4);
    db.Clients.Add(cl5);
    db.SaveChanges();
    Console.WriteLine("Клиенты добавлены!");
}

if (db.Accounts.FirstOrDefault() == null)
{
    var cl = db.Clients.ToList();
    foreach (var c in cl)
    {
        db.Accounts.Add(new Account { Name = "Новый счет", Client = c, ClientsID = c.Id });
    }
    db.SaveChanges();
    Console.WriteLine("Счета добавлены!");
}

if (db.Сards.FirstOrDefault() == null)
{
    var Acc = db.Accounts.ToList();
    foreach (var a in Acc)
    {
        db.Сards.Add(new Сard { Name = "Новая карта", Account = a, AccountID = a.Id});
    }
    db.SaveChanges();
    Console.WriteLine("Карты добавлены!");
}

Console.WriteLine("Клиенты:");
var selectClients = db.Clients.ToList();
foreach (var c in selectClients)
{
    Console.WriteLine($"{c.Id}.{c.Name} - {c.Email}");
}

Console.WriteLine("Счета:");
var selectAccounts = db.Accounts.ToList();
foreach (var a in selectAccounts)
{
    Console.WriteLine($"{a.Id}.{a.Name}");
}

Console.WriteLine("Карты:");
var selectСards = db.Сards.ToList();
foreach (var c in selectСards)
{
    Console.WriteLine($"{c.Id}.{c.Name}");
}


Console.WriteLine("Конец!");
