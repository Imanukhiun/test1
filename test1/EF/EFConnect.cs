﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using test1.Model;

namespace test1.EF
{
    public class EFConnect : DbContext
    {

        public DbSet<Client> Clients { get; set; }

        public DbSet<Сard> Сards { get; set; }

        public DbSet<Account> Accounts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=test;Trusted_Connection=True;Encrypt=False;Connection Timeout=300;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .HasOne(c => c.Client)
                .WithMany(a => a.Accounts)
                .HasForeignKey(f => f.ClientsID)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Сard>()
                .HasOne(x => x.Account)
                .WithMany(x => x.Сards)
                .HasForeignKey(f => f.AccountID)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Client>()
                .HasKey(x => x.Id);
        }
    }
}
